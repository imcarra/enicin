import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:enicin/login.dart';
import 'package:enicin/main.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';


class Register extends StatefulWidget {
  static const String id = "registro";
  @override
  _RegisterState createState() => _RegisterState();
}

class _RegisterState extends State<Register> {
  String email;
  String password;

  final FirebaseAuth _auth = FirebaseAuth.instance;
  final Firestore _firestore = Firestore.instance;

  Future<void> registroUsuario() async {

    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString('email', email);
    prefs.setString('password', password);

    AuthResult usuario = await _auth.createUserWithEmailAndPassword(
        email: email,
        password: password,
    );

    await _firestore.collection('usuarios').add({
      'email': email,
      'password': password
    });

    Navigator.push(context, MaterialPageRoute(
        builder: (context) => EnicinChat(usuario: usuario,)
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Enicin Chat"),
      ),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: <Widget>[
          Expanded(
            child: Hero(
              tag: 'logoEnicin',
              child: Container(
                child: Image.asset("assets/images/Blazed.jpg"),
              ),
            )
          ),
          SizedBox(
            height: 40.0,
          ),
          TextField(
            keyboardType: TextInputType.emailAddress,
            onChanged: (value) => email = value,
            decoration: InputDecoration(
              hintText: "Introduce tu email",
              border: const OutlineInputBorder(),
            ),
          ),
          SizedBox(
            height: 40.0,
          ),
          TextField(
            autocorrect: false,
            obscureText: true,
            onChanged: (value) => password = value,
            decoration: InputDecoration(
              hintText: "Contraseña",
              border: const OutlineInputBorder(),
            ),
          ),
          BotonesPersonalizados(
            texto: 'Registrate',
            callBack: () async{
              await registroUsuario();
          },
          )
        ],
      ),
    );
  }
}
