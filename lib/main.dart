import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:enicin/checkLogin.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'login.dart';

//void main() => runApp(Enicin());

CheckLogin ck = new CheckLogin();

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  Widget _defaultHome = new Enicin();

  AuthResult user = await ck.login();
  if(user != null){
   _defaultHome = new EnicinChat(usuario: user,);
  }
  debugPrint(user.toString());

  runApp(new MaterialApp(
    title: 'prueba',
    home: _defaultHome,
    routes: <String, WidgetBuilder>{
      'chat': (BuildContext context) => new EnicinChat(),
      'login': (BuildContext context) => new Enicin()
    },
  ));

}

class EnicinContactos extends StatefulWidget {
  @override
  _EnicinContactosState createState() => _EnicinContactosState();
}

class _EnicinContactosState extends State<EnicinContactos> {
  @override
  Widget build(BuildContext context) {
    return Container();
  }
}


class EnicinChat extends StatefulWidget {
  static const String id = "chat";
  final AuthResult usuario;

  const EnicinChat({Key key, this.usuario}) : super(key: key);
  @override
  _EnicinChatState createState() => _EnicinChatState();
}

class _EnicinChatState extends State<EnicinChat> {
  final FirebaseAuth _auth = FirebaseAuth.instance;
  final Firestore _firestore = Firestore.instance;

  TextEditingController messageController = TextEditingController();
  ScrollController scrollController = ScrollController();
  
  List<MenuItems> menuItemss = const <MenuItems>[
    const MenuItems('Configuracion', Icons.settings),
    const MenuItems('Desconectar',Icons.exit_to_app),
  ];

  Future<void> callback() async {
    if(messageController.text.length > 0) {
      await _firestore.collection('mensajes').add({
        'texto': messageController.text,
        'origen': widget.usuario.user.email,
        'fechaEnvio': DateTime.now().millisecondsSinceEpoch,
      });
      messageController.clear();
      scrollController.animateTo(
          scrollController.position.maxScrollExtent,
          duration: const Duration(milliseconds: 300),
          curve: Curves.easeOut
      );
    }
  }

  void onItemMenuPress(MenuItems items) {
    if (items.texto == 'Desconectar') {
      _auth.signOut();
      ck.logout();
      Navigator.of(context).pushAndRemoveUntil(
          MaterialPageRoute(builder: (context) => Enicin()),
              (Route <dynamic> route) => false);
    } else {
      //Navigator.push(context, MaterialPageRoute(builder: (context) => Configuracion()));
    }
  }
  
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: Hero(
          tag: 'enicinLogo',
          child: Container(
            height: 40.0,
            child: Image.asset("assets/images/Blazed.jpg"),
          ),
        ),
        title: Text('Enicin Chat'),
        actions: <Widget>[
          PopupMenuButton<MenuItems>(
            onSelected: onItemMenuPress,
            itemBuilder: (BuildContext context){
              return menuItemss.map((MenuItems mi){
                return PopupMenuItem<MenuItems>(
                  value: mi,
                  child: Row(
                    children: <Widget>[
                      Icon(
                        mi.icono,
                      ),
                      Container(
                        width: 10.0,
                      ),
                      Text(
                        mi.texto,
                        style: TextStyle(),
                      )
                    ],
                  ),
                );
              }).toList();
            },
          ),
        ],
      ),
      body: SafeArea(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Expanded(
              child: StreamBuilder<QuerySnapshot>(
                stream: _firestore.collection('mensajes').orderBy('fechaEnvio').snapshots(includeMetadataChanges: true),
                builder: (context, snapshot){
                  if(!snapshot.hasData)
                    return Center(
                      child: CircularProgressIndicator(),
                    );

                  List<DocumentSnapshot> docs = snapshot.data.documents;

                  List<Widget> mensajes = docs.map((doc) => Mensaje(
                    origen: doc.data['origen'],
                    texto: doc.data['texto'],
                    usuarioConectado: widget.usuario.user.email == doc.data['origen'],
                  )).toList();

                  return ListView(
                    controller: scrollController,
                    children: <Widget>[
                      ...mensajes,
                    ],
                  );
                },
            ),
            ),
            Container(
              child: Row(
                children: <Widget>[
                  Expanded(
                    child: Container(
                      width: 150.0,
                      height: 60.0,
                      padding: EdgeInsets.fromLTRB(10.0, 0, 0, 10.0),
                      child: TextField(
                        onSubmitted: (value) => callback(),
                        controller: messageController,
                        textInputAction: TextInputAction.send,
                        decoration: InputDecoration(
                            contentPadding: EdgeInsets.all(10.0),
                            hintText: 'Escribe...',
                            border: const OutlineInputBorder(
                            borderRadius: BorderRadius.all(Radius.circular(30.0)),
                          )
                        ),
                      ),
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.fromLTRB(0, 0, 0, 10.0),
                      child: BotonEnviar(
                      texto: 'Enviar',
                      callback: () => callback(),
                    ),
                  )
                ],
              ),
            ),

          ],
        ),
      ),
    );
  }
}

class MenuItems {
  final String texto;
  final IconData icono;

  const MenuItems(this.texto, this.icono);
}

class BotonEnviar extends StatelessWidget{
  final String texto;
  final VoidCallback callback;

  const BotonEnviar({Key key, this.texto, this.callback}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return FlatButton(
        color: Colors.orange,
        onPressed: callback,
        splashColor: Colors.white70,
        shape: new CircleBorder(),
        child: RawMaterialButton(
          onPressed: callback,
          padding: EdgeInsets.fromLTRB(6.0, 0, 0, 0),
          splashColor: Colors.white70,
          shape: new CircleBorder(),
          child: Icon(
            Icons.send,
            color: Colors.grey,
            size: 32.0,
          ),
      ),
    );
  }
}

class Mensaje extends StatelessWidget {
  final String origen;
  final String texto;

  final bool usuarioConectado;

  const Mensaje({Key key, this.origen, this.texto, this.usuarioConectado}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        crossAxisAlignment: usuarioConectado ? CrossAxisAlignment.end : CrossAxisAlignment.start,
        children: <Widget>[
          Text(
            origen,
          ),
          Material(
            color: usuarioConectado ? Colors.white70 : Colors.orangeAccent,
            borderRadius: BorderRadius.circular(10.0),
            elevation: 6.0,
            child: Container(
              padding: EdgeInsets.symmetric(vertical: 10.0, horizontal: 15.0),
              child: Text(
                  texto
              ),
            ),
          )
        ],
      ),
    );
  }
}