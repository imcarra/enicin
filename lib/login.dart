import 'package:enicin/register.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'main.dart';

class Enicin extends StatelessWidget{

  @override
  Widget build(BuildContext context) {


    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Enicin chat',
      initialRoute: MyEnicinLogin.id,
      routes: {
        MyEnicinLogin.id: (context) => MyEnicinLogin(),
        Register.id: (context) => Register(),
        Login.id: (context) => Login(),
        EnicinChat.id: (context) => EnicinChat(),
      },
    );
  }


}


class MyEnicinLogin extends StatelessWidget {
  static const String id = "idUser";

  @override
  Widget build(BuildContext context) {
      return Scaffold(
        body: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Hero(
                  tag: 'logoEnicin',
                  child: Container(
                    width: 250.0,
                    child: Image.asset("assets/images/Blazed.jpg"),
                  ),
                ),

              ],
            ),
            SizedBox(
              height: 60.0,
            ),
            Text(
              'Enicin Chat',
              style: TextStyle(
                fontSize: 40.0,
              ),
            ),
            SizedBox(
              height: 50.0,
            ),
            BotonesPersonalizados(
              texto: "Acceder",
              callBack: () {Navigator.of(context).pushNamed(Login.id);},
            ),
            BotonesPersonalizados(
              texto: "¿No estas registrado? Registrate!",
              callBack: () {Navigator.of(context).pushNamed(Register.id);},
            )
          ],
        ),
      );
  }

}

class BotonesPersonalizados extends StatelessWidget {
  final VoidCallback callBack;
  final String texto;

  const BotonesPersonalizados({Key key, this.callBack, this.texto}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.all(8.0),
      child: Material(
        color: Colors.lightBlue,
        elevation: 6.0,
        borderRadius: BorderRadius.circular(30.0),
        child: MaterialButton(
          onPressed: callBack,
          minWidth: 200.0,
          height: 45.0,
          child: Text(texto),
        ),
      ),
    );
  }
}

class Login extends StatefulWidget {
  static const String id = "login";
  @override
  _LoginState createState() => _LoginState();
}

class _LoginState extends State<Login> {

  String email;
  String password;

  final FirebaseAuth _auth = FirebaseAuth.instance;

  Future<void> accesoUsuario() async {

    AuthResult usuario = await _auth.signInWithEmailAndPassword(
      email: email,
      password: password,
    );

    Navigator.push(context, MaterialPageRoute(
        builder: (context) => EnicinChat(usuario: usuario,)
    ),
    );
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Enicin Chat"),
      ),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: <Widget>[
          Expanded(
              child: Hero(
                tag: 'logoEnicin',
                child: Container(
                  child: Image.asset("assets/images/Blazed.jpg"),
                ),
              )
          ),
          SizedBox(
            height: 40.0,
          ),
          TextField(
            keyboardType: TextInputType.emailAddress,
            onChanged: (value) => email = value,
            decoration: InputDecoration(
              hintText: "Introduce tu email",
              border: const OutlineInputBorder(),
            ),
          ),
          SizedBox(
            height: 40.0,
          ),
          TextField(
            autocorrect: false,
            obscureText: true,
            onChanged: (value) => password = value,
            decoration: InputDecoration(
              hintText: "Contraseña",
              border: const OutlineInputBorder(),
            ),
          ),
          BotonesPersonalizados(
            texto: 'Acceder',
            callBack: () async{
              await accesoUsuario();
            },
          )
        ],
      ),
    );
  }
}

