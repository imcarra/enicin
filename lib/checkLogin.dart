
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:shared_preferences/shared_preferences.dart';

class CheckLogin {

  final Firestore _firestore = Firestore.instance;
  final FirebaseAuth _auth = FirebaseAuth.instance;


  Future<AuthResult> login() async{
    SharedPreferences prefs = await SharedPreferences.getInstance();
    bool logged = false;
    bool userLogged = false;
    AuthResult usuario;


    String correo;
    String pass;

    var email = prefs.getString('email');
    var password = prefs.getString('password');

    debugPrint(email);
    debugPrint(password);

    if(email != null && password != null){

      logged = true;

    await _firestore.collection('usuarios').getDocuments().then((QuerySnapshot snapshot) {
      snapshot.documents.forEach((f){

        if(f.data['email'] == email && f.data['password'] == password){

          correo = f.data['email'];
          pass = f.data['password'];
          userLogged = true;
        }
      });
    });

      if(userLogged == true){
          usuario = await _auth.signInWithEmailAndPassword(
          email: correo,
          password: pass,
        );
      }
      else{
        usuario = null;
      }


      //DocumentReference docRef = _firestore.collection('usuarios').document()

      //_firestore.collection('usuarios').document().snapshots().forEach(DocumentSnapshot);

      //_firestore.collection('usuarios').document('idDispositivo').collection('datos').snapshots().toList();

      //Future<List<QuerySnapshot>> emailsAsociadosDispositivos = _firestore.collection('usuarios').document('idDispositivo').collection('datos').snapshots().toList();

    }
    else{

      logged = false;


    }

    return usuario;
  }

  Future<AuthResult> logout() async{
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.remove('email');
    prefs.remove('password');
  }

}